using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using RoleBasedAccessControl.Models;

namespace RoleBasedAccessControl.Policies
{
    public class DynamicPermissionRequirement : IAuthorizationRequirement
    {
        
    }

    public class DynamicPermissionHandler : AuthorizationHandler<DynamicPermissionRequirement>
    {
        private readonly Ctx _ctx;
        
        public DynamicPermissionHandler(Ctx ctx)
        {
            _ctx = ctx;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, DynamicPermissionRequirement requirement)
        {
            if(context.User == null)
                return Task.CompletedTask;
            
            var coh = context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;

            var temp = _ctx.Users.Include(ru => ru.RoleUser)
                .ThenInclude(r => r.Role)
                .ThenInclude(pr => pr.PermissionRole)
                .ThenInclude(p => p.Permission)
                .FirstOrDefault(u => u.CharacterOwnerHash == coh);

            if (temp == null)
                return Task.CompletedTask;

            var tempRoles = temp.RoleUser.ToList();
            
            if (tempRoles.Where(r => r.Role.IsSysAdmin).ToList().Count > 0)
            {
                context.Succeed(requirement);
            }
            
//            if (resource != null)
//            {
//                if (tempRoles.Exists(r => r.Role.Name == resource.Role))
//                {
//                    context.Succeed(requirement);
//                }
//            }

            if (!(context.Resource is AuthorizationFilterContext mvcContext))
                return Task.CompletedTask;

            var actionDescriptor = mvcContext.ActionDescriptor;
            var area = actionDescriptor.RouteValues["area"];
            var controller = actionDescriptor.RouteValues["controller"];
            var action = actionDescriptor.RouteValues["action"];
            var requestedResource = $"{area}-{controller}-{action}";

            foreach (var permission in tempRoles.Select(pr => pr.Role.PermissionRole))
            {
                if (permission.Exists(p => p.Permission.PermissionDescription == requestedResource))
                {
                    context.Succeed(requirement);
                }
            }
            
            return Task.CompletedTask;
        }
    }
}


//            var serializedRoles = context.User.Claims.FirstOrDefault(x => x.Type == "roles");
//            if (serializedRoles != null)
//            {
//                var roles = JsonConvert.DeserializeObject<User>(serializedRoles.Value);
//            }