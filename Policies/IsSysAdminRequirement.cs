using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using RoleBasedAccessControl.Models;

namespace RoleBasedAccessControl.Policies
{
    public class IsSysAdminRequirement : IAuthorizationRequirement {}
    
    public class IsSysAdminHandler : AuthorizationHandler<IsSysAdminRequirement>
    {
        private readonly Ctx _ctx;

        public IsSysAdminHandler(Ctx ctx)
        {
            _ctx = ctx;
        }
        
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, IsSysAdminRequirement requirement)
        {
            if(context.User == null)
                return Task.CompletedTask;
            
            var coh = context.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;

            var temp = _ctx.Users.Include(ru => ru.RoleUser)
                .ThenInclude(r => r.Role)
                .FirstOrDefault(u => u.CharacterOwnerHash == coh);

            if (temp == null)
                return Task.CompletedTask;

            var tempRoles = temp.RoleUser.ToList();
            
            if (tempRoles.Where(r => r.Role.IsSysAdmin).ToList().Count > 0)
            {
                context.Succeed(requirement);
            }

            return Task.CompletedTask;
        }
    }
}