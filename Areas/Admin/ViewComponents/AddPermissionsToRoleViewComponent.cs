using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RoleBasedAccessControl.Models;

namespace RoleBasedAccessControl.Areas.Admin.ViewComponents
{
    public class AddPermissionsToRoleViewComponent : ViewComponent
    {
        private readonly Ctx _ctx;

        public AddPermissionsToRoleViewComponent(Ctx ctx)
        {
            _ctx = ctx;
        }

        public async Task<IViewComponentResult> InvokeAsync(string id)
        {
            var permissions = await _ctx.Permissions.ToListAsync();
            var rolePermissions = await _ctx.Roles.Include(pr => pr.PermissionRole)
                .ThenInclude(p => p.Permission)
                .FirstAsync(r => r.RoleId == id);
            
            var unusedPermissions = new List<SelectListItem>();
            foreach (var permission in permissions)
            {
                if (rolePermissions.PermissionRole.Exists(p => p.Permission == permission)) continue;
                var temp = new SelectListItem()
                {
                    Value = permission.PermissionId.ToString(),
                    Text = permission.PermissionDescription
                };
                unusedPermissions.Add(temp);
            }

            var result = new PermissionViewModel()
            {
                Permissions = unusedPermissions,
                RoleId = id
            };
            
            return View(result);
        }
    }
}