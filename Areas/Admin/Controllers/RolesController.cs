﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RoleBasedAccessControl.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RoleBasedAccessControl.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Policy = "DynamicPermissions")]
    public class RolesController : Controller
    {
        private readonly Ctx _ctx;
        
        public RolesController(Ctx ctx)
        {
            _ctx = ctx;
        }
        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            var roles = await _ctx.Roles.ToListAsync();
            return View(roles);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Role role)
        {
            if (ModelState.IsValid)
            {
               await _ctx.Roles.AddAsync(role);
               await _ctx.SaveChangesAsync();
               return RedirectToAction("Index");
            }

            return View(role);
        }

        [HttpGet]
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var role = await _ctx.Roles.FindAsync(id);
            if (role == null)
            {
                return NotFound();
            }
            return View(role);
        }
        
        [HttpGet]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var role = await _ctx.Roles.FindAsync(id);
            if (role == null)
            {
                return NotFound();
            }
            return View(role);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Role role)
        {
            if (ModelState.IsValid)
            {
                _ctx.Update(role);
                await _ctx.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(role);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return BadRequest();
            }
            var role = await _ctx.Roles.FindAsync(id);
            if (role == null)
            {
                return NotFound();
            }
            return View(role);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var role = await _ctx.Roles.FindAsync(id);
            _ctx.Roles.Remove(role);
            await _ctx.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddUserToRole(RoleViewModel model)
        {
            var user = await _ctx.Users.Include(ru => ru.RoleUser).ThenInclude(r => r.Role)
                .FirstOrDefaultAsync(u => u.UserId == model.UserId);
            var role = await _ctx.Roles.FirstOrDefaultAsync(r => r.RoleId == model.Role);

            if (user.RoleUser.Exists(r => r.RoleId == model.Role))
                return RedirectToAction("Edit", "Users", new {id = model.UserId});
            
            var newRole = new RoleUser()
            {
                Role = role,
                RoleId = model.Role,
                UserId = model.UserId,
                User = user
            };
            user.RoleUser.Add(newRole);
            _ctx.Update(user);
            await _ctx.SaveChangesAsync();

            return RedirectToAction("Edit", "Users", new {id = model.UserId});
        }

        public async Task<IActionResult> DeleteUserFromRole(int id, string roleId)
        {
            var user = await _ctx.Users.Include(ru => ru.RoleUser).ThenInclude(r => r.Role)
                .FirstOrDefaultAsync(u => u.UserId == id);
            var role = user.RoleUser.FirstOrDefault(r => r.RoleId == roleId);

            user.RoleUser.Remove(role);
            await _ctx.SaveChangesAsync();
            
            return RedirectToAction("Edit", "Users", new {id = id});
        }
    }
}
