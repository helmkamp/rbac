using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RoleBasedAccessControl.Models;

namespace RoleBasedAccessControl.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Policy = "DynamicPermissions")]
    public class UsersController : Controller
    {
        private readonly Ctx _ctx;
        
        public UsersController(Ctx ctx)
        {
            _ctx = ctx;
        }
        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            var users = await _ctx.Users.ToListAsync();
            return View(users);
        }

        [HttpGet]
        public async Task<IActionResult> Details(int id)
        {
            if (id == 0)
            {
                return BadRequest();
            }
            var user = await _ctx.Users.FirstOrDefaultAsync(u => u.UserId == id);
            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }
        
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            if (id == 0)
            {
                return BadRequest();
            }
            var user = await _ctx.Users.Include(ru => ru.RoleUser).ThenInclude(r => r.Role)
                .FirstOrDefaultAsync(u => u.UserId == id);
            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(User user)
        {
            if (ModelState.IsValid)
            {
                _ctx.Update(user);
                await _ctx.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(user);
        }

        [HttpGet]
        public async Task<IActionResult> Delete(int id)
        {
            if (id == 0)
            {
                return BadRequest();
            }
            var user = await _ctx.Users.FirstOrDefaultAsync(u => u.UserId == id);
            if (user == null)
            {
                return NotFound();
            }
            return View(user);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var user = await _ctx.Users.FirstOrDefaultAsync(u => u.UserId == id);
            _ctx.Users.Remove(user);
            await _ctx.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}