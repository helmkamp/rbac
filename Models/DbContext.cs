﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RoleBasedAccessControl.Models
{
    public class Ctx : DbContext
    {
        public Ctx(DbContextOptions options) : base(options) { }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Permission> Permissions { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<RoleUser>()
                .HasKey(t => new { t.UserId, t.RoleId });

            modelBuilder.Entity<RoleUser>()
                .HasOne(r => r.User)
                .WithMany(u => u.RoleUser)
                .HasForeignKey(ru => ru.UserId);

            modelBuilder.Entity<RoleUser>()
                .HasOne(u => u.Role)
                .WithMany(r => r.RoleUser)
                .HasForeignKey(ru => ru.RoleId);

            modelBuilder.Entity<PermissionRole>().
                HasKey(t => new { t.RoleId, t.PermissionId });

            modelBuilder.Entity<PermissionRole>()
                .HasOne(r => r.Permission)
                .WithMany(p => p.PermissionRole)
                .HasForeignKey(pr => pr.PermissionId);

            modelBuilder.Entity<PermissionRole>()
                .HasOne(p => p.Role)
                .WithMany(r => r.PermissionRole)
                .HasForeignKey(pr => pr.RoleId);
                
        }
    }
}
