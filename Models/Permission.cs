﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace RoleBasedAccessControl.Models
{
    public class Permission
    {
        [Key]
        public int PermissionId { get; set; }
        [Required]
        [StringLength(75)]
        [Display(Name = "Description")]
        public string PermissionDescription { get; set; }

        public List<PermissionRole> PermissionRole { get; set; }
    }

    public class PermissionViewModel
    {
        public string Permission { get; set; }
        public List<SelectListItem> Permissions { get; set; }
        public string RoleId { get; set; }
    }
}
